module.exports = function(grunt) {

  grunt.initConfig({
    sass: {
        options: {
            sourceMap: false,
            outputStyle: 'compressed',
            sourceComments: false,
        },
        dist: {
            files: {
                'css/button.css': 'sass/main.scss'
            }
        }
    }
  });

  grunt.loadNpmTasks('grunt-sass');

  grunt.registerTask('default', ['sass']);

};