<?php

// include 'inc/minify.php';
include 'inc/loginize.php';


function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css' );
    wp_enqueue_style( 'ui-style', get_stylesheet_directory_uri() . '/css/ui.css' );
    wp_enqueue_style( 'buttons-style', get_stylesheet_directory_uri() . '/css/button.css' );
    wp_enqueue_style( 'portfolio-style', get_stylesheet_directory_uri() . '/css/portfolio.css' );
    wp_enqueue_script( 'video', get_stylesheet_directory_uri() . '/js/fitvid.js' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );


?>