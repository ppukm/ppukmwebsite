<?php

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/yooinc.png);
            padding-bottom: 10px;			
        }
    </style>
<?php }

add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Login Yoo CMS';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

function my_login_stylesheet() {
    wp_enqueue_style('custom-style', get_stylesheet_directory_uri() . '/login/login.css');
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

function personal_admin_footer( $footer_text ) {
	global $wp_version;
	/* Don't change the footer text in the network admin */
	if ( ! is_network_admin() ) {
		/* The way of determining the default footer text was changed in 3.9 */
		if ( version_compare( $wp_version, '3.9', '<' ) ) {
			$old_text = __( 'Thank you for creating with <a href="http://wordpress.org/">WordPress</a>.' );
		} else {
			$old_text = sprintf( __( 'Thank you for creating with <a href="%s">WordPress</a>.' ), __( 'https://wordpress.org/' ) );
		}
		/* Define the new footer text */
		$new_text = __( 'This Site Build With <a href="https://bogordesain.com">Bogor Desain</a>.', 'personal-admin-footer' );
		/* Add the site name and link to the new footer text */
		$new_text = sprintf ( $new_text, get_home_url(), get_bloginfo( 'name' ) );
		/* Replace the old text with the new text */
		$footer_text = str_replace( $old_text, $new_text, $footer_text );
	}
	return $footer_text;
}
add_filter( 'admin_footer_text', 'personal_admin_footer' );

// Update CSS within in Admin
/*
function admin_style() {
  wp_enqueue_style('admin-styles', get_stylesheet_directory_uri().'/login/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');
*/

